<?php
$_['heading_title'] = 'XSL';

//Текст внутри настроек модуля
$_['text_module'] = 'Модули';
$_['text_edit'] = 'Настройки модуля';
$_['entry_status'] = 'Статус';

$_['text_success'] = 'Успіх';
$_['text_warning'] = 'Помилка';
$_['text_prais'] = 'Прайс-лист';
$_['text_name'] = 'Назва';
$_['text_image'] = 'Фото';
$_['text_name_category'] = 'Назва розділу';
$_['text_model'] = 'Модель';
$_['text_artikul'] = 'Ціна/Ціна акційна';
$_['text_col'] = 'Кількість на складі';

$_['text_button_create'] = 'Створитит';
$_['text_button_update'] = 'Обновити';
$_['text_button_download'] = 'Скачати XSl';
$_['text_button_delete'] = 'Видалити XSL';
