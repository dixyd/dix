

<?php
class ControllerToolExel extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('module/exel');
		$data = $this->launguageInit();

		//подключаем наш языковой файл
		//$data['heading_title'] = $this->language->get('heading_title');
		$this->document->setTitle($this->language->get('heading_title'));
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$data['action'] = $this->url->link('tool/exel', 'token=' . $this->session->data['token'], 'SSL');
		$data['action_download'] = $this->url->link('tool/exel/download', 'token=' . $this->session->data['token'], 'SSL');
		$data['action_delete'] = $this->url->link('tool/exel/delete', 'token=' . $this->session->data['token'], 'SSL');
		$this->load->model('tool/exel');
		$data['success'] = "";
		$data['warning'] = "";

		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
			$this->model_tool_exel->exele();
			$data['success'] = $this->language->get('success');

		}
		$this->response->setOutput($this->load->view('tool/exel.tpl', $data));
	}

	public function download() {
		$file = "xsl/namee.xls";
		if (file_exists($file)) {
			// сбрасываем буфер вывода PHP, чтобы избежать переполнения памяти выделенной под скрипт
			// если этого не сделать файл будет читаться в память полностью!
			if (ob_get_level()) {
				ob_end_clean();
			}
			// заставляем браузер показать окно сохранения файла
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename=' . basename($file));
			header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($file));
			// читаем файл и отправляем его пользователю
			readfile($file);
			exit;
		}
	}
	public function delete() {
		$file = "xsl/namee.xls";
		if (file_exists($file)) {

			unlink($file);
			$this->response->redirect($this->url->link('tool/exel', 'token=' . $this->session->data['token'], 'SSL'));

		}
	}

	protected function launguageInit() {

		$data['text_button_create'] = $this->language->get('text_button_create');
		$data['text_button_update'] = $this->language->get('text_button_update');
		$data['text_button_download'] = $this->language->get('text_button_download');
		$data['text_button_delete'] = $this->language->get('text_button_delete');
		$data['text_button_create'] = $this->language->get('text_button_create');
		return $data;

	}

}