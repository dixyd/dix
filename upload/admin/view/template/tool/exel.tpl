<?php echo $header; ?><?php echo $column_left; ?>



<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo "XML"; ?></h1>
   
   
  	<div class="container-fluid"><div class="panel-body">
<?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>

    <?php if ($warning) { ?>
    <div class="alert alert-warning"><i class="fa fa-check-circle"></i> <?php echo $warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
  	<div class="well">
<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-product" class="form-horizontal">
  <div class="form-group">
    <div class="col-sm-offset-5 col-sm-10"> 
    <?php if(file_exists("xsl/namee.xls")){ ?>
    <p class="navbar-btn">
                    <a href="<?=$action_download?>" class="btn btn-default"><?=$text_button_download?></a>
                </p>
      <button type="submit" class="btn btn-default"><?=$text_button_update?></button>
       <p class="navbar-btn">
                    <a href="<?=$action_delete?>" class="btn btn-default"><?=$text_button_delete?> </a>
                </p>
      <?php }else{ ?>
         <button type="submit" class="btn btn-default"><?=$text_button_create?></button>
        <?php } ?>

    </div>
  </div>
</form></div></div></div>
</div>

<?php echo $footer; ?>