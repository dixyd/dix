<?php

class ModelToolExel extends Model {

	public function exele() {
		$cwd = getcwd();
		chdir(DIR_SYSTEM . 'PHPExcel');
		require_once 'Classes/PHPExcel.php';
		chdir($cwd);

// Создаем объект класса PHPExcel
		$xls = new PHPExcel();
// Устанавливаем индекс активного листа
		$xls->setActiveSheetIndex(0);
// Получаем активный лист
		$sheet = $xls->getActiveSheet();
// Подписываем лист
		$sheet->setTitle('Прайс-лист');

// Вставляем текст в ячейку A1
		$sheet->setCellValue("A1", 'Прайс-лист');
		$sheet->getStyle('A1')->getFill()->setFillType(
			PHPExcel_Style_Fill::FILL_SOLID);
		$sheet->getStyle('A1')->getFill()->getStartColor()->setRGB('EEEEEE');

// Объединяем ячейки
		$sheet->mergeCells('A1:H2');

// Выравнивание текста
		$sheet->getStyle('A1')->getAlignment()->setHorizontal(
			PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$array = array("A", "B", "C", "D", "E", "F", "G");
		$sheet->getRowDimension('3')->setRowHeight(20);
		foreach ($array as $value) {
			$sheet->getColumnDimension($value)->setWidth(30);
			$sheet->getColumnDimension($value);

		}
		$data = $this->getProductDescrip();
		$data_category = $this->getProdyuctCategority();
		//var_dump($data);
		$sheet->setCellValue('A3', 'Назва');
		$sheet->setCellValue('B3', 'Фото');
		$sheet->setCellValue('C3', 'Назва розділу');
		$sheet->setCellValue('D3', 'Модель');
		$sheet->setCellValue('E3', 'Артикул');
		$sheet->setCellValue('F3', 'Ціна/Ціна акційна');
		$sheet->setCellValue('G3', 'Кількість на складі');
		$i = 4;
		//var_dump($data[1]);
		//echo dirname(__FILE__);
		foreach ($data as $value) {
			//var_dump($value);
			//echo "<br><br>";
			$discount = $value["price"] . $this->discount($value["product_id"]);
			$sheet->setCellValueByColumnAndRow(0, $i, $value["name"]);
			$this->files(DIR_IMAGE . $value["image"], $i, $sheet);
			$sheet->setCellValueByColumnAndRow(2, $i, $data_category[$value["category_id"]]["name"]);
			$sheet->setCellValueByColumnAndRow(3, $i, $value["model"]);
			$sheet->setCellValueByColumnAndRow(4, $i, $value["sku"]);
			$sheet->setCellValueByColumnAndRow(5, $i, $discount);
			$sheet->setCellValueByColumnAndRow(6, $i, $value["quantity"]);
			for ($i_a = 0; $i_a <= 6; $i_a++) {
				$sheet->getStyleByColumnAndRow($i_a, $i)->getAlignment()->setHorizontal(
					PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			}
			$sheet->getRowDimension($i)->setRowHeight(50);
			$sheet->getRowDimension($i)->setOutlineLevel(1);
			$sheet->getRowDimension($i)->setCollapsed(true);
			$i++;
		}

		$sheet->getStyle('A3:G3')->getFill()->setFillType(
			PHPExcel_Style_Fill::FILL_SOLID);
		$sheet->getStyle('A3:G3')->getFill()->getStartColor()->setRGB('E0E0E0');
		$sheet->getStyle('A3:G3')->getAlignment()->setHorizontal(
			PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$objWriter = new PHPExcel_Writer_Excel5($xls);
		$objWriter->save('xsl/namee.xls');
		$this->session->data['success'] = "Успіх";

	}

	protected function getProductDescrip() {

		$query = $this->db->query("SELECT a.*,ad.name,sort.* FROM `" . DB_PREFIX . "product` AS a LEFT JOIN `" . DB_PREFIX . "product_description` AS ad  ON a.product_id=ad.product_id LEFT JOIN `" . DB_PREFIX . "product_to_category` AS sort  ON ad.product_id=sort.product_id WHERE ad.language_id=1 ORDER BY sort.category_id DESC ");
		//echo "erwtretretertterrererere<br>";
		return $query->rows;

	}

	protected function getProdyuctCategority() {

		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_description`");
		//$i_array = 0;

		foreach ($query->rows as $value) {
			$array_category[$value["category_id"]] = $value;
		}
		return $array_category;

	}
	protected function getProductDiscount() {

		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product_special`");
		foreach ($query->rows as $value) {
			$array_discount[$value["product_id"]] = $value;
		}
		return $array_discount;

	}

	protected function discount($id) {
		$data_discount = $this->getProductDiscount();
		if (isset($data_discount[$id])) {

			return "/ Знижка: " . $data_discount[$id]["price"];
		}

	}

	protected function files($patch, $cordinates, $sheet) {
		if (file_exists($patch)) {
			$image = new PHPExcel_Worksheet_Drawing();
			$image->setPath($patch);
			$image->setCoordinates("B" . $cordinates);
			$image->setOffsetX(0);
			$image->setOffsetY(0);
			$image->setHeight(40);
			$image->setWidth(100);
			$sheet->getStyle('B')->getAlignment()->setHorizontal(
				PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$image->setWorksheet($sheet);
		}

	}

}