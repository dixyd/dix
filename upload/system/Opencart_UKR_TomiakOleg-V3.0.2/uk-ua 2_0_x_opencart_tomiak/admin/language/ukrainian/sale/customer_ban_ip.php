<?php
// Heading
$_['heading_title']    = 'Список забанених IP';

// Text
$_['text_success']     = 'Налаштування успішно змеіені!';
$_['text_list']        = 'Список забанених IP';
$_['text_add']          = 'Добавити';
$_['text_edit']         = 'Редагування';

// Column
$_['column_ip']        = 'IP';
$_['column_customer']  = 'Клієнти';
$_['column_action']    = 'Дія';

// Entry
$_['entry_ip']         = 'IP';

// Error
$_['error_permission'] = 'У вас немає прав для зміни Списку забаених IP!';
$_['error_ip']         = 'IP повинен бути від 1 до 15 символів!';

