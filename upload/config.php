<?php
// HTTP
define('HTTP_SERVER', 'http://task.ua/opencart/upload/');

// HTTPS
define('HTTPS_SERVER', 'http://task.ua/opencart/upload/');

// DIR
define('DIR_APPLICATION', '/home/dio/Documents/project/task/opencart/upload/catalog/');
define('DIR_SYSTEM', '/home/dio/Documents/project/task/opencart/upload/system/');
define('DIR_LANGUAGE', '/home/dio/Documents/project/task/opencart/upload/catalog/language/');
define('DIR_TEMPLATE', '/home/dio/Documents/project/task/opencart/upload/catalog/view/theme/');
define('DIR_CONFIG', '/home/dio/Documents/project/task/opencart/upload/system/config/');
define('DIR_IMAGE', '/home/dio/Documents/project/task/opencart/upload/image/');
define('DIR_CACHE', '/home/dio/Documents/project/task/opencart/upload/system/storage/cache/');
define('DIR_DOWNLOAD', '/home/dio/Documents/project/task/opencart/upload/system/storage/download/');
define('DIR_LOGS', '/home/dio/Documents/project/task/opencart/upload/system/storage/logs/');
define('DIR_MODIFICATION', '/home/dio/Documents/project/task/opencart/upload/system/storage/modification/');
define('DIR_UPLOAD', '/home/dio/Documents/project/task/opencart/upload/system/storage/upload/');

// DB
define('DB_DRIVER', 'mpdo');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'osctore');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
